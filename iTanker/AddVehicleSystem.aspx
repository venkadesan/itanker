﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="AddVehicleSystem.aspx.cs" Inherits="iTanker.AddVehicleSystem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    
     <script language="javascript">
       <!--
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Transaction"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Vehicle System"></asp:Literal></li>
    </ul>
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Add Vehicle System</h4>
        <div class="form-box-content">
            <asp:Panel runat="server" ID="pAdd">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="vehcile" runat="server" ID="lStatusName" />:</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:DropDownList ID="ddlvehicle" CssClass="select-chosen form-control" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:Label Text="Building:" runat="server" ID="Label3" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:DropDownList ID="ddlbuilding" CssClass="select-chosen form-control" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="In Time" runat="server" ID="Label1" />:</label>
                    <div class="col-md-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtindate" CssClass="form-control input-datepicker " runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtintime" CssClass="form-control input-timepicker" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <asp:Label Text="Out Time:" runat="server" ID="Label2" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtoutdate" CssClass="form-control input-datepicker " runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtouttime" CssClass="form-control input-timepicker" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Tokken ID" runat="server" ID="Label4" />:</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:TextBox ID="txttokenid" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-10 col-md-offset-2">
                        <asp:Button ID="bSave" Text="Save" runat="server" ToolTip="Click button to save record"
                            TabIndex="3" class="btn btn-success" onclick="bSave_Click" />
                        <asp:Button ID="bCancel" Text="Cancel" runat="server" ToolTip="Click button to cancel record"
                            TabIndex="4" class="btn btn-danger" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
