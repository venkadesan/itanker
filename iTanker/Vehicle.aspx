﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Vehicle.aspx.cs" Inherits="iTanker.Vehicle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <script>
        $(document).ready(function () {
            /* Initialize Datatables */
            $('#example-datatables').dataTable();
            $('.dataTables_filter input').attr('placeholder', 'Search');

            $('#livehicle').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#livehicle").parent().parent().parent().addClass("active");
        });
    </script>
    <script type="text/javascript">

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Tanker Type"></asp:Literal></li>
        <li>
            <asp:LinkButton ID="bNew" runat="server" ToolTip="Click button to add record" class="hi hi-plus"
                OnClick="bNew_Click" />
        </li>
    </ul>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Vehicle</h4>
        <div class="form-box-content">
            <asp:Panel runat="server" ID="pAdd">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Vendor" runat="server" ID="Label1" />:</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:DropDownList ID="ddlVendor" CssClass="select-chosen form-control" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:Label Text="Tanker:" runat="server" ID="Label4" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:DropDownList ID="ddtanker" CssClass="select-chosen form-control" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Regn. No" runat="server" ID="lStatusName" />:</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:TextBox ID="tRegnNo" runat="server" TabIndex="1" ToolTip="Please enter the Reg. No"
                                class="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:Label Text="Capacity:" runat="server" ID="Label3" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="tCapacity" runat="server" TabIndex="2" ToolTip="Please enter the Capacity"
                                class="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Default Building" runat="server" ID="Label8" />:</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:DropDownList ID="ddlbuilding" CssClass="select-chosen form-control" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-10 col-md-offset-2">
                        <asp:Button ID="bSave" Text="Save" runat="server" ToolTip="Click button to save record"
                            TabIndex="3" class="btn btn-success" OnClick="bSave_Click" />
                        <asp:Button ID="bCancel" Text="Cancel" runat="server" ToolTip="Click button to cancel record"
                            TabIndex="4" class="btn btn-danger" OnClick="bCancel_Click" />
                    </div>
                </div>
            </asp:Panel>
            <div class="row">
                <table id="example-datatables" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="submenu new-color">
                            <th style="width: 5%;">
                                S.No
                            </th>
                            <th style="width: 20%;" class="cell-small text-center hidden-xs hidden-sm">
                                Vendor
                            </th>
                            <th style="width: 20%;" class="cell-small text-center hidden-xs hidden-sm">
                                Tanker
                            </th>
                            <th style="width: 30%;">
                                Regn. No
                            </th>
                            <th style="width: 10%;" class="cell-small text-center hidden-xs hidden-sm">
                                Capacity
                            </th>
                            <th style="width: 10%; text-align: center;">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrvehicle" runat="server" OnItemCommand="rptrStatus_ItemCommand"
                        DataSourceID="">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td class="cell-small text-center hidden-xs hidden-sm">
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>' />
                                </td>
                                <td class="cell-small text-center hidden-xs hidden-sm">
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TankerType") %>' ToolTip='<%# Eval("TankerType") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("VehicleRegNo") %>' ToolTip='<%# Eval("VehicleRegNo") %>' />
                                </td>
                                <td class="cell-small text-center hidden-xs hidden-sm">
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Capacity") %>' ToolTip='<%# Eval("Capacity") %>' />
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <asp:LinkButton ID="imgEdit" class="btn btn-xs btn-success" CommandArgument='<%# Eval("VehicleID") %>'
                                            ToolTip="Edit" CommandName="edit" runat="server"> <i class="fa fa-pencil"></i></asp:LinkButton>
                                        <asp:LinkButton ID="imgDelete" class="btn btn-xs btn-danger" CommandArgument='<%# Eval("VehicleID") %>'
                                            ToolTip="Delete" OnClientClick="return confirm('Are you sure to delete?');" CommandName="delete"
                                            runat="server"> <i class="fa fa-times"></i></asp:LinkButton>
                                        <asp:HiddenField runat="server" ID="hStatusID" Value='<%# Eval("VehicleID")%>' />
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
