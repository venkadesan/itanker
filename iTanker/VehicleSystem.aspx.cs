﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerBL;
using iTankerPL;

namespace iTanker
{
    public partial class VehicleSystem : PageBase
    {
        PLVehicleSystem _objPlVehicleSystem = new PLVehicleSystem();
        BLVehicleSystem _objBlVehicleSystem = new BLVehicleSystem();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                LoadDetails();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            try
            {
                divmsg.Visible = true;
                lblError.Text = message;
                if (et == Common.ErrorType.Error)
                {
                    divmsg.Attributes.Add("class", "alert alert-danger");
                }
                else if (et == Common.ErrorType.Information)
                {
                    divmsg.Attributes.Add("class", "alert alert-success");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Transaction";
                if (submenu.Trim() == string.Empty)
                    submenu = "Vehicle System";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }


        public void LoadDetails()
        {
            _objPlVehicleSystem.CompanyID = UserSession.CompanyIDCurrent;
            _objPlVehicleSystem.LocationID = UserSession.LocationIDCurrent;
            DataSet Ds = new DataSet();
            Ds = _objBlVehicleSystem.GetVehicleSystems(_objPlVehicleSystem);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrvehicleSystem.DataSource = Ds;
                rptrvehicleSystem.DataBind();
            }
            else
            {
                rptrvehicleSystem.DataSource = null;
                rptrvehicleSystem.DataBind();
            }
        }

        //public string DayCalculation(int secs)
        //{
        //    try
        //    {
        //        var hours = (secs / 3600);
        //        var minutes = (secs - (hours * 3600)) / 60;
        //        var seconds = secs - (hours * 3600) - (minutes * 60);

        //        if (hours < 10) { hours = "0" + hours; }
        //        if (minutes < 10) { minutes = "0" + minutes; }
        //        if (seconds < 10) { seconds = "0" + seconds; }
        //        var time = hours + ':' + minutes + ':' + seconds;
        //    }
        //    catch (Exception ex)
        //    {
        //        NotifyMessages(ex.Message, Common.ErrorType.Error);
        //        return "0";
        //    }
        //}

        public string DayCalculation(object minute)
        {
            try
            {


                int iValues = int.Parse(minute.ToString());

                TimeSpan span = TimeSpan.FromSeconds(iValues);
                string result = String.Format("{0} Days : {1} Hrs : {2} Mins : {3} Sec",
    span.Days, span.Hours, span.Minutes, span.Seconds);

                //if (iValues < 0)
                //    iValues = -1 * iValues;

                //int iDays = iValues / (24 * 60 * 60);

                //iValues = int.Parse(Math.IEEERemainder(iValues, (24 * 60)).ToString());
                //int iHours = iValues / (60 * 60);

                //iValues = int.Parse(Math.IEEERemainder(iValues, 60).ToString());
                //int iMinutes = iValues / 60;

                //string result = iDays.ToString() + "Day " + iHours.ToString() + " Hrs " + iMinutes.ToString() + " Mins";
                return result;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
                return "0";
            }
        }
    }
}