﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="VehicleSystem.aspx.cs" Inherits="iTanker.VehicleSystem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <script>
        $(document).ready(function () {
            /* Initialize Datatables */
            $('#example-datatables').dataTable();
            $('.dataTables_filter input').attr('placeholder', 'Search');

            $('#livehiclesystem').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#livehiclesystem").parent().parent().parent().addClass("active");
        });
    </script>
    <script>
        function Daycalulcation(secs) {
            alert('test');
            var hours = Math.floor(secs / 3600);
            var minutes = Math.floor((secs - (hours * 3600)) / 60);
            var seconds = secs - (hours * 3600) - (minutes * 60);

            if (hours < 10) { hours = "0" + hours; }
            if (minutes < 10) { minutes = "0" + minutes; }
            if (seconds < 10) { seconds = "0" + seconds; }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        }
    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Transaction"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Vehcile System"></asp:Literal></li>
    </ul>
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Vehicle System</h4>
        <div class="form-box-content">
            <div class="row">
                <table id="example-datatables" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="submenu new-color">
                            <th>
                                S.No
                            </th>
                            <th>
                                Vendor Name
                            </th>
                            <th>
                                Commodity Type
                            </th>
                            <th>
                                Reg. No
                            </th>
                            <th>
                                Capacity
                            </th>
                            <th>
                                Building
                            </th>
                            <th>
                                In Time
                            </th>
                            <th>
                                Out Time
                            </th>
                             <th>
                                Duration
                            </th>
                            <th>
                                System Date
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrvehicleSystem" runat="server" DataSourceID="">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TankerType") %>' ToolTip='<%# Eval("TankerType") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("VehicleRegNo") %>' ToolTip='<%# Eval("VehicleRegNo") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Capacity") %>' ToolTip='<%# Eval("Capacity") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("BuildingName") %>' ToolTip='<%# Eval("BuildingName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("InTime") %>' ToolTip='<%# Eval("InTime") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("OutTime") %>' ToolTip='<%# Eval("OutTime") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label9" runat="server" Text='<%# DayCalculation(Eval("TotalSeconds")) %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Date") %>' ToolTip='<%# Eval("Date") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
