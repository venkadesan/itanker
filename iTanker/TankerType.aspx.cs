﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerPL;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;

namespace iTanker
{
    public partial class TankerType : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }



        #endregion

        PLTankerType _objpltankertype = new PLTankerType();
        BLTankerType _objbltankertype = new BLTankerType();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                LoadDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
                Setheadertextdetails();
            }
        }
        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Master";
                if (submenu.Trim() == string.Empty)
                    submenu = "Tanker Type";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        public void LoadDetails()
        {
            _objpltankertype.CompanyID = UserSession.CompanyIDCurrent;
            _objpltankertype.LocationID = UserSession.LocationIDCurrent;
            _objpltankertype.TypeId = 0;
            DataSet Ds = new DataSet();
            Ds = _objbltankertype.GetTankerType(_objpltankertype);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrtankerType.DataSource = Ds;
                rptrtankerType.DataBind();
            }
            else
            {
                rptrtankerType.DataSource = null;
                rptrtankerType.DataBind();
            }
        }

        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            _objpltankertype.TypeId = this.ID.Value;

            _objpltankertype.CompanyID = UserSession.CompanyIDCurrent;
            _objpltankertype.LocationID = UserSession.LocationIDCurrent;
            Ds = _objbltankertype.GetTankerType(_objpltankertype);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tTankerType.Text = Ds.Tables[0].Rows[0]["TankerType"].ToString();
                this.tShortName.Text = Ds.Tables[0].Rows[0]["TankerShortname"].ToString();
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bCancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            if (status == Common.ButtonStatus.New || status == Common.ButtonStatus.Cancel ||
                status == Common.ButtonStatus.Edit)
            {
                this.tTankerType.Text = tShortName.Text = string.Empty;
            }
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tTankerType.Focus();
            }
            else if (status == Common.ButtonStatus.View)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tTankerType.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tTankerType.Focus();
                this.pAdd.Visible = true;

                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }

        protected void rptrStatus_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                _objpltankertype.TypeId = this.ID.Value;
                string message = _objbltankertype.DeleteTankerType(_objpltankertype);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }


        protected void bSave_Click(object sender, EventArgs e)
        {
            if (tTankerType.Text == string.Empty)
            {
                NotifyMessages("Please enter the Tanker Type", Common.ErrorType.Error);
                return;
            }
            _objpltankertype.CompanyID = UserSession.CompanyIDCurrent;
            _objpltankertype.LocationID = UserSession.LocationIDCurrent;
            _objpltankertype.Type = this.tTankerType.Text.Trim();
            _objpltankertype.ShortType = this.tShortName.Text.Trim();
            _objpltankertype.Cuid = UserSession.UserID;
            try
            {
                if (this.IsAdd)
                {
                    _objpltankertype.TypeId = 0;
                    string[] Values = _objbltankertype.InsertUpdateTankerType(_objpltankertype);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Error);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else if (this.ID != null)
                {
                    _objpltankertype.TypeId = this.ID.Value;
                    string[] Values = _objbltankertype.InsertUpdateTankerType(_objpltankertype);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Error);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }
    }
}