﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerPL;
using Kowni.BusinessLogic;

namespace iTanker
{
    public partial class ThemeSelection : PageBase
    {
        PLThemeselection objplTheme = new PLThemeselection();
        BLThemeSelection objblTheme = new BLThemeSelection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindColorThemes();
            }

        }

        private void BindColorThemes()
        {
            try
            {
                objplTheme.CompanyID = UserSession.CompanyIDCurrent;
                DataSet dsThemes = new DataSet();
                dsThemes = objblTheme.GetColorThemes(objplTheme);
                if (dsThemes != null && dsThemes.Tables.Count > 0 && dsThemes.Tables[0].Rows.Count > 0)
                {
                    DataRow drthemes = dsThemes.Tables[0].Rows[0];
                    ddlprimary.SelectedValue = drthemes["primaryTheme"].ToString();
                    ddlsecondary.SelectedValue = drthemes["secondaryTheme"].ToString();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                string primary = ddlprimary.SelectedItem.Value;
                string secondary = ddlsecondary.SelectedValue;

                objplTheme.CompanyID = UserSession.CompanyIDCurrent;
                objplTheme.PrimaryTheme = primary;
                objplTheme.SecondaryTheme = secondary;

                string[] values = objblTheme.InsertUpdateColorThemes(objplTheme);
                if (values[1] == "1")
                {
                    BindColorThemes();
                    NotifyMessages(values[0], Common.ErrorType.Information);
                    Response.Redirect("ThemeSelection.aspx");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }

        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }
    }
}