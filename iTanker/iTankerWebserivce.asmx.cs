﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using iTanker.Class;
using iTanker.Code;
using iTankerBL;
using iTankerPL;
using Kowni.BusinessLogic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text;
using System.Configuration;
using System.IO;
using System.Net;
using System.Xml;

namespace iTanker
{
    /// <summary>
    /// Summary description for iTankerWebserivce
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class iTankerWebserivce : System.Web.Services.WebService
    {
        Kowni.Common.BusinessLogic.BLSingleSignOn objSingleSignOn = new Kowni.Common.BusinessLogic.BLSingleSignOn();
        Kowni.Common.BusinessLogic.BLAnnouncement objAnnouncement = new Kowni.Common.BusinessLogic.BLAnnouncement();

        BLLocation _objLocation = new BLLocation();
        BLCompany _objCompany = new BLCompany();

        PLVehicle _objPlVehicle = new PLVehicle();
        BLVehicle _objBlVehicle = new BLVehicle();

        PLTankerType _objpltankertype = new PLTankerType();
        BLTankerType _objbltankertype = new BLTankerType();

        PLVendor _objplvendor = new PLVendor();
        BLVendor _objblvendor = new BLVendor();

        PLVehicleSystem _objPlVehicleSystem = new PLVehicleSystem();
        BLVehicleSystem _objBlVehicleSystem = new BLVehicleSystem();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        #region "Check Login Credentials"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetLoginDetails(string username, string password)
        {
            string _res = string.Empty;

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Username or Password", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            try
            {
                int RoleID = 0;
                int UserID = 0;
                int CompanyID = 0;
                string CompanyName = string.Empty;
                int LocationID = 0;
                string LocationName = string.Empty;
                int GroupID = 0;
                int LanguageID = 0;
                string UserFName = string.Empty;
                string UserLName = string.Empty;
                string UserMailID = string.Empty;
                string ThemeFolderPath = string.Empty;

                if (!objSingleSignOn.Login(username, password, 53, out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "Unauthorized Access.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
                else
                {
                    Model.User lstUser = new Model.User();
                    lstUser.UserID = UserID;
                    lstUser.CompanyID = CompanyID;
                    lstUser.LocationID = LocationID;
                    lstUser.CompanyName = CompanyName;
                    lstUser.LocationName = LocationName;
                    lstUser.CompanyShortName = string.Empty;
                    lstUser.LocationShortName = string.Empty;
                    lstUser.Message = "Login Success";
                    lstUser.Status = true;

                    DataSet dscompanyDet = new DataSet();
                    dscompanyDet = _objCompany.GetCompany(CompanyID);
                    if (dscompanyDet != null && dscompanyDet.Tables.Count > 0 && dscompanyDet.Tables[0].Rows.Count > 0)
                    {
                        DataRow drcompanyDet = dscompanyDet.Tables[0].Rows[0];
                        lstUser.CompanyShortName = drcompanyDet["companyShortName"].ToString();
                    }

                    DataSet dslocDet = new DataSet();
                    dslocDet = _objLocation.GetLocation_ByID(LocationID);
                    if (dslocDet != null && dslocDet.Tables.Count > 0 && dslocDet.Tables[0].Rows.Count > 0)
                    {
                        DataRow drlocDet = dslocDet.Tables[0].Rows[0];
                        lstUser.LocationShortName = drlocDet["LocationShortName"].ToString();
                    }

                    Model.ResponseDataNew rd = new Model.ResponseDataNew();
                    rd.Data = lstUser;

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //string result = serializer.Serialize(lstUser);

                    List<object> resultMains = new List<object>();
                    object resultMain = new object();
                    resultMain = lstUser;
                    resultMains.Add(resultMain);
                    string results = serializer.Serialize(resultMains);
                    return results;
                }
            }
            catch (Exception)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            return _res;
        }

        #endregion

        #region "Vehicle Details"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetVehicleDetails(int companyid, int locationid)
        {
            string _res = string.Empty;

            if (string.IsNullOrWhiteSpace(companyid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(companyid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (string.IsNullOrWhiteSpace(locationid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(locationid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            try
            {
                _objPlVehicle.CompanyID = companyid;
                _objPlVehicle.LocationID = locationid;
                _objPlVehicle.VehicleId = 0;
                DataSet Ds = new DataSet();
                Ds = _objBlVehicle.GetVehicle(_objPlVehicle);

                List<Model.VehicleData> listVehicle = new List<Model.VehicleData>();
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drVehicle in Ds.Tables[0].Rows)
                    {
                        Model.VehicleData objvehicle = new Model.VehicleData();
                        objvehicle.VehicleId = IntVal(drVehicle["VehicleID"]);
                        objvehicle.RegNo = StrVal(drVehicle["VehicleRegNo"]);
                        objvehicle.Capacity = StrVal(drVehicle["Capacity"]);
                        objvehicle.VendorId = IntVal(drVehicle["VendorID"]);
                        objvehicle.TankerId = IntVal(drVehicle["TankerID"]);
                        objvehicle.DefaultBuildingId = IntVal(drVehicle["DefaultBuildingID"]);
                        objvehicle.VendorName = StrVal(drVehicle["VendorName"]);
                        objvehicle.CommodityType = StrVal(drVehicle["TankerType"]);
                        listVehicle.Add(objvehicle);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = listVehicle;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion

        #region "Building Details"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetBuildingDetails(int locationid)
        {
            string _res = string.Empty;

            if (string.IsNullOrWhiteSpace(locationid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(locationid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            try
            {
                _objPlVehicle.LocationID = locationid;
                DataSet dsBuilding = new DataSet();
                dsBuilding = _objBlVehicle.GetBuildingDetails(_objPlVehicle);

                List<Model.BuildingData> lstBuildingDatas = new List<Model.BuildingData>();
                if (dsBuilding.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drVehicle in dsBuilding.Tables[0].Rows)
                    {
                        Model.BuildingData objBuildings = new Model.BuildingData();
                        objBuildings.BuildingID = IntVal(drVehicle["BuildingID"]);
                        objBuildings.BuildingName = StrVal(drVehicle["BuildingName"]);
                        objBuildings.BuildingShortName = StrVal(drVehicle["BuildingShortName"]);
                        lstBuildingDatas.Add(objBuildings);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstBuildingDatas;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion

        #region "Add Vehicle System"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string InsertVehicleSystem(int vehicleId, int tokenId, string dtTime, string type, string dtDay, int buildingId, int companyid, int locationId, int Cuid, string guid)
        {
            string _res = string.Empty;
            try
            {
                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime datTime = DateTime.ParseExact(dtTime, "d/M/yyyy HH:mm:ss", culture);

                DateTime datDay = DateTime.ParseExact(dtDay, "d/M/yyyy HH:mm:ss", culture);

                _objPlVehicleSystem.VehicleId = vehicleId;
                _objPlVehicleSystem.TokenId = tokenId;
                _objPlVehicleSystem.SystemDate = datDay;
                _objPlVehicleSystem.Type = type;
                _objPlVehicleSystem.SystemTime = datTime;
                _objPlVehicleSystem.BuildingId = buildingId;
                _objPlVehicleSystem.CompanyID = companyid;
                _objPlVehicleSystem.LocationID = locationId;
                _objPlVehicleSystem.Cuid = Cuid;
                _objPlVehicleSystem.InGuid = guid;
                string[] values = _objBlVehicleSystem.InsertUpdateVehicle(_objPlVehicleSystem);

                if (values[1] != "-1")
                {
                    _res = JsonConvert.SerializeObject(new Model.VehicleStatusData() { Message = values[0], Status = true, Guid = guid }, Newtonsoft.Json.Formatting.Indented);
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.VehicleStatusData() { Message = values[0], Status = false, Guid = guid }, Newtonsoft.Json.Formatting.Indented);
                }


                return _res;
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false }, Newtonsoft.Json.Formatting.Indented);
                UpdateLog("Vehilce System: "+_res);
            }
            return _res;
        }

        private void UpdateLog(string errormessge)
        {
            try
            {
                string filename = DateTime.Now.ToString("yyyyMMdd");
                StreamWriter sw = File.AppendText(HttpContext.Current.Server.MapPath("~/Log/" + filename + "Log.log"));
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " :: " + errormessge);
                sw.AutoFlush = true;
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            catch
            {
            }
        }


        #endregion

        #region Get In-details

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetInVehicleDetails(int companyid, int locationid, DateTime inDateTime)
        {
            string _res = string.Empty;
            if (string.IsNullOrWhiteSpace(companyid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(companyid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (string.IsNullOrWhiteSpace(locationid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(locationid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }



            try
            {
                _objPlVehicle.LocationID = locationid;
                _objPlVehicle.CompanyID = companyid;
                _objPlVehicle.Cdate = inDateTime;
                DataSet dsBuilding = new DataSet();
                dsBuilding = _objBlVehicle.GetInVehiclesDetails(_objPlVehicle);

                List<Model.InVehicleData> lstBuildingDatas = new List<Model.InVehicleData>();
                if (dsBuilding.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drVehicle in dsBuilding.Tables[0].Rows)
                    {
                        Model.InVehicleData objBuildings = new Model.InVehicleData();
                        objBuildings.VehicleId = IntVal(drVehicle["VehcileID"]);
                        objBuildings.TokkenId = IntVal(drVehicle["TokkenID"]);
                        objBuildings.RegNo = StrVal(drVehicle["VehicleRegNo"]);
                        objBuildings.InTime = Convert.ToString(drVehicle["InTime"]);
                        objBuildings.Capacity = StrVal(drVehicle["Capacity"]);
                        objBuildings.VendorName = StrVal(drVehicle["VendorName"]);
                        objBuildings.BuildingName = StrVal(drVehicle["BuildingName"]);
                        objBuildings.ShowInDate = StrVal(drVehicle["ShowInDate"]);
                        objBuildings.ShowInTime = StrVal(drVehicle["ShowInTime"]);
                        objBuildings.VendorId = IntVal(drVehicle["VendorID"]);
                        objBuildings.BuildingId = IntVal(drVehicle["BuildingID"]);
                        objBuildings.BuildingShortName = StrVal(drVehicle["BuildingShortName"]);
                        lstBuildingDatas.Add(objBuildings);
                    }
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    object resultMain = new object();
                    resultMain = lstBuildingDatas;
                    string results1 = serializer.Serialize(resultMain);
                    return results1;
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion


        #region "Vendor Details"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetVendorDetails(int companyid, int locationid)
        {
            string _res = string.Empty;

            if (string.IsNullOrWhiteSpace(companyid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(companyid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (string.IsNullOrWhiteSpace(locationid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(locationid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            try
            {
                _objplvendor.CompanyID = companyid;
                _objplvendor.LocationID = locationid;
                _objplvendor.VendorId = 0;
                DataSet Ds = new DataSet();
                Ds = _objblvendor.GetVendor(_objplvendor);
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    return ConvertJavaSeriptSerializer(Ds.Tables[0]);
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.ToString(), Status = false }, Newtonsoft.Json.Formatting.Indented);
                UpdateLog("Vendor Add"+_res);
            }
            return _res;
        }

        #endregion


        #region "Tanker-Type Details"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string GetTankerType(int companyid, int locationid)
        {
            string _res = string.Empty;

            if (string.IsNullOrWhiteSpace(companyid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(companyid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Company.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (string.IsNullOrWhiteSpace(locationid.ToString()))
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            if (IsNumeric(locationid.ToString()) == false)
            {
                return JsonConvert.SerializeObject(new Model.StatusData() { Message = "Invalid Location.", Status = false }, Newtonsoft.Json.Formatting.Indented);
            }

            try
            {
                _objpltankertype.CompanyID = companyid;
                _objpltankertype.LocationID = locationid;
                _objpltankertype.TypeId = 0;
                DataSet Ds = new DataSet();
                Ds = _objbltankertype.GetTankerType(_objpltankertype);
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    return ConvertJavaSeriptSerializer(Ds.Tables[0]);
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "No Record Found.", Status = false }, Newtonsoft.Json.Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                _res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.ToString(), Status = false }, Newtonsoft.Json.Formatting.Indented);
            }
            return _res;
        }

        #endregion


        #region "Add Vehicle"

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string AddVehicle(int companyid, int locationId, int buildingId, int Cuid, string regno, string capacity, int vendorid, int tankertype)
        {
            string _res = string.Empty;
            try
            {
                _objPlVehicle.VendorId = vendorid;
                _objPlVehicle.Capacity = capacity;
                _objPlVehicle.TankerId = tankertype;
                _objPlVehicle.VehicleRegno = regno;
                _objPlVehicle.BuildingID = buildingId;
                _objPlVehicle.CompanyID = companyid;
                _objPlVehicle.LocationID = locationId;
                _objPlVehicle.Cuid = Cuid;
                _objPlVehicle.VehicleId = 0;
                string[] values = _objBlVehicle.InsertUpdateVehicle(_objPlVehicle);
                int vehicleId = Convert.ToInt32(values[2]);
                if (values[1] != "-1")
                {
                    _res = JsonConvert.SerializeObject(new Model.NewVehicleStatusData() { Message = values[0], Status = true, VehicleId = vehicleId }, Newtonsoft.Json.Formatting.Indented);
                }
                else
                {
                    _res = JsonConvert.SerializeObject(new Model.NewVehicleStatusData() { Message = values[0], Status = false, VehicleId = vehicleId }, Newtonsoft.Json.Formatting.Indented);
                }


                return _res;
            }
            catch (Exception ex)
            {
                UpdateLog("Add vehicle" + _res);
                _res = JsonConvert.SerializeObject(new Model.NewVehicleStatusData() { Message = "An Internal Error Occured. Please Try Again." + ex.Message, Status = false, VehicleId = 0 }, Newtonsoft.Json.Formatting.Indented);

            }
            return _res;
        }


        #endregion


        private string ConvertJavaSeriptSerializer(DataTable dt)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            List<object> resultMain = new List<object>();
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    result.Add(column.ColumnName, "" + row[column.ColumnName]);
                }
                resultMain.Add(result);
            }
            return serializer.Serialize(resultMain);
        }

        public int IntVal(object val)
        {
            int output = 0;
            if (val != null && int.TryParse(val.ToString(), out output))
            {
                return output;
            }

            return 0;
        }

        public long LongVal(object val)
        {
            long output = 0;
            if (val != null && long.TryParse(val.ToString(), out output))
            {
                return output;
            }

            return 0;
        }

        public string StrVal(object val)
        {
            if (val != null)
            {
                return val.ToString();
            }

            return string.Empty;
        }

        public bool BoolVal(object val)
        {
            bool output = false;
            if (val != null && bool.TryParse(val.ToString(), out output))
            {
                return output;
            }

            return false;
        }

        #region "Common Function"

        public bool IsNumeric(string strVal)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9-]");
            System.Text.RegularExpressions.Regex reg2 = new System.Text.RegularExpressions.Regex("^-[0-9]+$|^[0-9]+$");
            return (!reg.IsMatch(strVal) && reg2.IsMatch(strVal));
        }

        public bool IsDate(string strVal)
        {
            bool IsDate = Regex.IsMatch(strVal, @"^(\d{2})/\d{2}/(\d{4})$");
            return IsDate;
        }

        public bool IsEmail(string strVal)
        {
            const string regExpr = @"^(?("")("".+?""@)|" +
                  @"(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)" +
                  @"(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])" +
                  @"|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$";
            System.Text.RegularExpressions.Match m = System.Text.RegularExpressions.Regex.Match(strVal, regExpr);
            if (m.Success) return true;
            else return false;
        }

        public static string Encryption(string inputStr)
        {
            try
            {
                byte[] encData_byte = new byte[inputStr.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(inputStr);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in Encryption" + ex.Message);
            }
        }

        public static string Decryption(string inputStr)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();
                byte[] todecode_byte = Convert.FromBase64String(inputStr);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length); char[] decoded_char = new char[charCount]; utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in Decryption" + ex.Message);
            }
        }


        #endregion
    }
}
