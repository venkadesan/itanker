﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerPL;
using Kowni.Framework.Enumeration;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;

namespace iTanker
{
    public partial class AddVehicleSystem : PageBase
    {
        PLVehicle _objPlVehicle = new PLVehicle();
        BLVehicle _objBlVehicle = new BLVehicle();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindBuilding();
                LoadVehicle();
            }
        }

        private void BindBuilding()
        {
            try
            {
                _objPlVehicle.CompanyID = UserSession.CompanyIDCurrent;
                _objPlVehicle.LocationID = UserSession.LocationIDCurrent;
                _objPlVehicle.VehicleId = 0;
                DataSet dsBuilding = new DataSet();
                dsBuilding = _objBlVehicle.GetBuildingDetails(_objPlVehicle);
                if (dsBuilding.Tables[0].Rows.Count > 0)
                {
                    ddlbuilding.DataSource = dsBuilding.Tables[0];
                    ddlbuilding.DataValueField = "BuildingID";
                    ddlbuilding.DataTextField = "BuildingName";
                    ddlbuilding.DataBind();
                }
                else
                {
                    ddlbuilding.DataSource = null;
                    ddlbuilding.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }

        }

        public void LoadVehicle()
        {
            _objPlVehicle.CompanyID = UserSession.CompanyIDCurrent;
            _objPlVehicle.LocationID = UserSession.LocationIDCurrent;
            _objPlVehicle.VehicleId = 0;
            DataSet Ds = new DataSet();
            Ds = _objBlVehicle.GetVehicle(_objPlVehicle);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                ddlvehicle.DataSource = Ds.Tables[0];
                ddlvehicle.DataValueField = "VehicleID";
                ddlvehicle.DataTextField = "VehicleRegNo";
                ddlvehicle.DataBind();
            }
            else
            {
                ddlvehicle.DataSource = null;
                ddlvehicle.DataBind();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {

                int vehicleid = 0, buildingid = 0;
                if (!Common.DDVal(ddlvehicle, out vehicleid))
                {
                    NotifyMessages("Select vehicle", Common.ErrorType.Error);
                    return;
                }
                if (!Common.DDVal(ddlbuilding, out buildingid))
                {
                    NotifyMessages("Select Building", Common.ErrorType.Error);
                    return;
                }
                if (txtindate.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter InDate", Common.ErrorType.Error);
                    return;
                }
                if (txtintime.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter InTime", Common.ErrorType.Error);
                    return;
                }
                if (txtoutdate.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter outDate", Common.ErrorType.Error);
                    return;
                }
                if (txtouttime.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter outTime", Common.ErrorType.Error);
                    return;
                }
                if (txttokenid.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter Token", Common.ErrorType.Error);
                    return;
                }
                string indate = txtindate.Text.Trim();
                string intime = txtintime.Text.Trim();

                string outdate = txtoutdate.Text.Trim();
                string outtime = txtouttime.Text.Trim();

                int tokenid = Convert.ToInt32(txttokenid.Text);

                DateTime inDateTime = DateTime.ParseExact(indate + " " + intime, "M/d/yyyy h:mm tt", CultureInfo.InvariantCulture);
                DateTime outDateTime = DateTime.ParseExact(outdate + " " + outtime, "M/d/yyyy h:mm tt", CultureInfo.InvariantCulture);


                //DateTime inDateTime = Convert.ToDateTime(indate + "" + intime);
                //DateTime outDateTime = Convert.ToDateTime(outdate + "" + outtime);
                PLVehicleSystem objPlVehicleSystem = new PLVehicleSystem();
                BLVehicleSystem objBlVehicleSystem = new BLVehicleSystem();
                objPlVehicleSystem.BuildingId = buildingid;
                objPlVehicleSystem.CompanyID = UserSession.CompanyIDCurrent;
                objPlVehicleSystem.LocationID = UserSession.LocationIDCurrent;
                objPlVehicleSystem.SystemDate = outDateTime;
                objPlVehicleSystem.SystemTime = inDateTime;
                objPlVehicleSystem.TokenId = tokenid;
                objPlVehicleSystem.VehicleId = vehicleid;

                string[] Values = objBlVehicleSystem.InsertUpdateVehicleSystemWeb(objPlVehicleSystem);
                if (Convert.ToInt32(Values[1]) == -1)
                {
                    NotifyMessages(Values[0], Common.ErrorType.Error);
                }
                else
                {
                    NotifyMessages(Values[0], Common.ErrorType.Information);
                    ClearControls();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private void ClearControls()
        {
            txtindate.Text = string.Empty;
            txtintime.Text = string.Empty;
            txtoutdate.Text = string.Empty;
            txtouttime.Text = string.Empty;
            txttokenid.Text = string.Empty;
        }
    }
}