﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerPL;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;

namespace iTanker
{
    public partial class Vendor : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }



        #endregion

        PLVendor _objplvendor = new PLVendor();
        BLVendor _objblvendor = new BLVendor();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
                Setheadertextdetails();
            }
        }
        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Master";
                if (submenu.Trim() == string.Empty)
                    submenu = "Vendor";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        public void LoadDetails()
        {
            _objplvendor.CompanyID = UserSession.CompanyIDCurrent;
            _objplvendor.LocationID = UserSession.LocationIDCurrent;
            _objplvendor.VendorId = 0;
            DataSet Ds = new DataSet();
            Ds = _objblvendor.GetVendor(_objplvendor);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrvendor.DataSource = Ds;
                rptrvendor.DataBind();
            }
            else
            {
                rptrvendor.DataSource = null;
                rptrvendor.DataBind();
            }
        }

        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            _objplvendor.VendorId = this.ID.Value;

            _objplvendor.CompanyID = UserSession.CompanyIDCurrent;
            _objplvendor.LocationID = UserSession.LocationIDCurrent;
            Ds = _objblvendor.GetVendor(_objplvendor);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tvendorname.Text = Ds.Tables[0].Rows[0]["VendorName"].ToString();
                this.tvendoraddress.Text = Ds.Tables[0].Rows[0]["VendorAddress"].ToString();
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bCancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            if (status == Common.ButtonStatus.New || status == Common.ButtonStatus.Cancel ||
                status == Common.ButtonStatus.Edit)
            {
                this.tvendorname.Text = tvendoraddress.Text = string.Empty;
            }
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tvendorname.Focus();
            }
            else if (status == Common.ButtonStatus.View)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tvendorname.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tvendorname.Focus();
                this.pAdd.Visible = true;

                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }

        protected void rptrStatus_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                _objplvendor.VendorId = this.ID.Value;
                string message = _objblvendor.DeleteVendor(_objplvendor);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }


        protected void bSave_Click(object sender, EventArgs e)
        {
            if (tvendorname.Text == string.Empty)
            {
                NotifyMessages("Please enter the Tanker Type", Common.ErrorType.Error);
                return;
            }
            _objplvendor.CompanyID = UserSession.CompanyIDCurrent;
            _objplvendor.LocationID = UserSession.LocationIDCurrent;
            _objplvendor.VendorName = this.tvendorname.Text.Trim();
            _objplvendor.VendorAddress = this.tvendoraddress.Text.Trim();
            _objplvendor.Cuid = UserSession.UserID;
            try
            {
                if (this.IsAdd)
                {
                    _objplvendor.VendorId = 0;
                    string[] Values = _objblvendor.InsertUpdateVendor(_objplvendor);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Error);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else if (this.ID != null)
                {
                    _objplvendor.VendorId = this.ID.Value;
                    string[] Values = _objblvendor.InsertUpdateVendor(_objplvendor);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Error);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }
    }
}
