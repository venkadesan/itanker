﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="ThemeSelection.aspx.cs" Inherits="iTanker.ThemeSelection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <script>
        $(document).ready(function () {

            $('#lithemeselection').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#lithemeselection").parent().parent().parent().addClass("active");
        });
    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Theme Selection"></asp:Literal></li>
    </ul>
    <div id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Asset Mapping</h4>
        <div class="form-box-content">
            <div id="theme-options" class="text-left visible-md visible-lg">
                <a href="javascript:void(0)" class="btn btn-theme-options open"><i class="fa fa-cog">
                </i>Theme Options</a>
                <div id="theme-options-content" style="display: block;">
                    <h5>
                        Primary Themes</h5>
                    <ul class="theme-colors clearfix">
                        <li><a href="javascript:void(0)" class="themed-background-Red themed-border-Red"
                            data-theme="default" data-toggle="tooltip" title="" data-original-title="Red"></a>
                        </li>
                        <li><a href="javascript:void(0)" class="themed-background-Orange themed-border-Orange"
                            data-theme="css/themes/deepblue.css" data-toggle="tooltip" title="" data-original-title="Orange">
                        </a></li>
                        <li><a href="javascript:void(0)" class="themed-background-black themed-border-black"
                            data-theme="css/themes/deepwood.css" data-toggle="tooltip" title="" data-original-title="black">
                        </a></li>
                        <li><a href="javascript:void(0)" class="themed-background-Midnight themed-border-Midnight"
                            data-theme="css/themes/deeppurple.css" data-toggle="tooltip" title="" data-original-title="Midnight">
                        </a></li>
                        <li><a href="javascript:void(0)" class="themed-background-Olive themed-border-Olive"
                            data-theme="css/themes/deepgreen.css" data-toggle="tooltip" title="" data-original-title="Olive">
                        </a></li>
                    </ul>
                    <asp:DropDownList ID="ddlprimary" Style="width: 15%;" CssClass="select-chosen" runat="server">
                        <Items>
                            <asp:ListItem Text="Red" Value="default"></asp:ListItem>
                            <asp:ListItem Text="Orange" Value="deepblue.css"></asp:ListItem>
                            <asp:ListItem Text="black" Value="deepwood.css"></asp:ListItem>
                            <asp:ListItem Text="Midnight" Value="deeppurple.css"></asp:ListItem>
                            <asp:ListItem Text="Olive" Value="deepgreen.css"></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </div>
            </div>
            <div id="Div1" class="text-left visible-md visible-lg">
                <a href="javascript:void(0)" class="btn clssecondary open" id="secondary"><i class="fa fa-cog">
                </i>Secondary Theme</a>
                <div id="theme-options-content-secondary" style="display: block;">
                    <h5>
                        Secondary Color Themes</h5>
                    <ul class="theme-colors clearfix">
                        <li><a href="javascript:void(0)" class="themed-background-black themed-border-black"
                            data-theme="secondary,css/themes/navdeepGray.css" data-toggle="tooltip" title=""
                            data-original-title="black"></a></li>
                        <li><a href="javascript:void(0)" class="themed-background-white themed-border-white"
                            data-theme="secondary,css/themes/navdeepWhite.css" data-toggle="tooltip" title=""
                            data-original-title="white"></a></li>
                        <li><a href="javascript:void(0)" class="themed-background-gold themed-border-gold"
                            data-theme="secondary,css/themes/navdeepGold.css" data-toggle="tooltip" title=""
                            data-original-title="gold"></a></li>
                        <li><a href="javascript:void(0)" class="themed-background-chalk themed-border-chalk"
                            data-theme="secondary,css/themes/navdeepChalk.css" data-toggle="tooltip" title=""
                            data-original-title="chalk"></a></li>
                    </ul>
                    <asp:DropDownList ID="ddlsecondary" Style="width: 15%;" CssClass="select-chosen"
                        runat="server">
                        <Items>
                            <asp:ListItem Text="black" Value="navdeepGray.css"></asp:ListItem>
                            <asp:ListItem Text="white" Value="navdeepWhite.css"></asp:ListItem>
                            <asp:ListItem Text="gold" Value="navdeepGold.css"></asp:ListItem>
                            <asp:ListItem Text="chalk" Value="navdeepChalk.css"></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <asp:Button ID="bSave" Text="Save" runat="server" ToolTip="Click button to save record"
                        TabIndex="3" class="btn btn-primary" OnClick="bSave_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
