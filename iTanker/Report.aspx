﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Report.aspx.cs" Inherits="iTanker.Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var lname = '<%= Session["LocationName"] %>';
            var dtitle = lname + ' Tanker Inward reports';
            $('#example-datatables').DataTable({
                dom: 'Bfrtip',
                bPaginate: false,
                buttons: [
            'copy', 'csv',
            {
                extend: 'excel',
                title: dtitle

            },
                        {
                            extend: 'pdf',
                            title: dtitle,
                            footer: true,
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                            },
                            sPdfOrientation: 'portrait'
                        }, 'print'
        ]
            });
        });

        doc['footer'] = (function (page, pages) {
            return {
                columns: [
'Left part of footer',
{
    alignment: 'right',
    text: [
{ text: page.toString(), italics: true },
' of ',
{ text: pages.toString(), italics: true }
]
}
],
                margin: [10, 0]
            }
        });

    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Report"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Report"></asp:Literal></li>
    </ul>
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Report
            <asp:Label ID="lblheader" runat="server" Text=""></asp:Label></h4>
        <div class="form-box-content">
            <div class="form-group">
                <div class="col-md-2">
                    From
                    <asp:TextBox ID="tfromdate" runat="server" data-date-format="dd/mm/yyyy" CssClass="form-control input-datepicker"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    To
                    <asp:TextBox ID="ttoDate" runat="server" data-date-format="dd/mm/yyyy" CssClass="form-control input-datepicker"></asp:TextBox>
                </div>
                <div class="col-md-3">
                    Vendor
                    <asp:DropDownList ID="ddlVendor" CssClass="select-chosen form-control" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-md-3">
                    Tanker Type
                    <asp:DropDownList ID="ddtanker" CssClass="select-chosen form-control" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-md-1">
                    <asp:LinkButton ID="lnksearch" runat="server" ToolTip="To Search with Filters" OnClick="lnksearch_Click"><i style="margin-top: 20px;" class="gemicon-medium-search"></i></asp:LinkButton>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-1" style="float: right">
                    <asp:LinkButton ID="lnkdownload" OnClick="lnkdownload_Click" runat="server" ToolTip="To Download XL Document"><i class="gemicon-medium-download"></i></asp:LinkButton>
                </div>
            </div>
            <div class="row">
                <table id="example-datatables" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="submenu new-color">
                            <th>
                                S.No
                            </th>
                            <th>
                                Vendor
                            </th>
                            <th>
                                Commodity Type
                            </th>
                            <th>
                                Reg. No
                            </th>
                            <th>
                                Capacity
                            </th>
                            <th>
                                Tower
                            </th>
                            <th>
                                System Date
                            </th>
                            <th>
                                In Time
                            </th>
                            <th>
                                Out Time
                            </th>
                            <th>
                                Duration
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrvehicleSystem" runat="server" DataSourceID="">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="count" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="VendorName" runat="server" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="TankerType" runat="server" Text='<%# Eval("TankerType") %>' ToolTip='<%# Eval("TankerType") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="VehicleRegNo" runat="server" Text='<%# Eval("VehicleRegNo") %>' ToolTip='<%# Eval("VehicleRegNo") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Capacity" runat="server" Text='<%# Eval("Capacity") %>' ToolTip='<%# Eval("Capacity") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="BuildingName" runat="server" Text='<%# Eval("Tower") %>' ToolTip='<%# Eval("Tower") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Date" runat="server" Text='<%# Eval("Date") %>' ToolTip='<%# Eval("Date") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="InTime" runat="server" Text='<%# Eval("InTime") %>' ToolTip='<%# Eval("InTime") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="OutTime" runat="server" Text='<%# Eval("OutTime") %>' ToolTip='<%# Eval("OutTime") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="TotalSeconds" runat="server" Text='<%# DayCalculation(Eval("TotalSeconds")) %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
