﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerPL;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;

namespace iTanker
{
    public partial class Vehicle : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }



        #endregion

        PLVehicle _objPlVehicle = new PLVehicle();
        BLVehicle _objBlVehicle = new BLVehicle();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
                Setheadertextdetails();
                BindVendor();
                BindTanker();
                BindBuilding();
            }
        }

        private void BindBuilding()
        {
            try
            {
                _objPlVehicle.CompanyID = UserSession.CompanyIDCurrent;
                _objPlVehicle.LocationID = UserSession.LocationIDCurrent;
                _objPlVehicle.VehicleId = 0;
                DataSet dsBuilding = new DataSet();
                dsBuilding = _objBlVehicle.GetBuildingDetails(_objPlVehicle);
                if (dsBuilding.Tables[0].Rows.Count > 0)
                {
                    ddlbuilding.DataSource = dsBuilding.Tables[0];
                    ddlbuilding.DataValueField = "BuildingID";
                    ddlbuilding.DataTextField = "BuildingName";
                    ddlbuilding.DataBind();
                }
                else
                {
                    ddlbuilding.DataSource = null;
                    ddlbuilding.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }

        }

        private void BindVendor()
        {
            try
            {
                PLVendor _objPlVendor = new PLVendor();
                BLVendor _objBlVendor = new BLVendor();

                DataSet dsVendor = new DataSet();
                _objPlVendor.CompanyID = UserSession.CompanyIDCurrent;
                _objPlVendor.LocationID = UserSession.LocationIDCurrent;
                _objPlVendor.VendorId = 0;
                dsVendor = _objBlVendor.GetVendor(_objPlVendor);
                if (dsVendor.Tables.Count > 0 && dsVendor.Tables[0].Rows.Count > 0)
                {
                    ddlVendor.DataSource = dsVendor.Tables[0];
                    ddlVendor.DataValueField = "VendorID";
                    ddlVendor.DataTextField = "VendorName";
                    ddlVendor.DataBind();
                }
                else
                {
                    ddlVendor.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindTanker()
        {
            try
            {
                PLTankerType _objPlTankerType = new PLTankerType();
                BLTankerType _objBlTankerType = new BLTankerType();

                DataSet dsVendor = new DataSet();
                _objPlTankerType.CompanyID = UserSession.CompanyIDCurrent;
                _objPlTankerType.LocationID = UserSession.LocationIDCurrent;
                _objPlTankerType.TypeId = 0;
                dsVendor = _objBlTankerType.GetTankerType(_objPlTankerType);
                if (dsVendor.Tables.Count > 0 && dsVendor.Tables[0].Rows.Count > 0)
                {
                    ddtanker.DataSource = dsVendor.Tables[0];
                    ddtanker.DataValueField = "TypeID";
                    ddtanker.DataTextField = "TankerType";
                    ddtanker.DataBind();
                }
                else
                {
                    ddtanker.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Master";
                if (submenu.Trim() == string.Empty)
                    submenu = "Vehicle";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        public void LoadDetails()
        {
            _objPlVehicle.CompanyID = UserSession.CompanyIDCurrent;
            _objPlVehicle.LocationID = UserSession.LocationIDCurrent;
            _objPlVehicle.VehicleId = 0;
            DataSet Ds = new DataSet();
            Ds = _objBlVehicle.GetVehicle(_objPlVehicle);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrvehicle.DataSource = Ds;
                rptrvehicle.DataBind();
            }
            else
            {
                rptrvehicle.DataSource = null;
                rptrvehicle.DataBind();
            }
        }

        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            _objPlVehicle.VehicleId = this.ID.Value;

            _objPlVehicle.CompanyID = UserSession.CompanyIDCurrent;
            _objPlVehicle.LocationID = UserSession.LocationIDCurrent;
            Ds = _objBlVehicle.GetVehicle(_objPlVehicle);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tRegnNo.Text = Ds.Tables[0].Rows[0]["VehicleRegNo"].ToString();
                this.tCapacity.Text = Ds.Tables[0].Rows[0]["Capacity"].ToString();
                this.ddlVendor.SelectedValue = Ds.Tables[0].Rows[0]["VendorID"].ToString();
                this.ddtanker.SelectedValue = Ds.Tables[0].Rows[0]["TankerID"].ToString();
                if (Ds.Tables[0].Rows[0]["DefaultBuildingID"].ToString() != string.Empty)
                {
                    this.ddlbuilding.SelectedValue = Ds.Tables[0].Rows[0]["DefaultBuildingID"].ToString();
                }
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bCancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            if (status == Common.ButtonStatus.New || status == Common.ButtonStatus.Cancel ||
                status == Common.ButtonStatus.Edit)
            {
                this.tCapacity.Text = tRegnNo.Text = string.Empty;
            }
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tRegnNo.Focus();
            }
            else if (status == Common.ButtonStatus.View)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tRegnNo.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tRegnNo.Focus();
                this.pAdd.Visible = true;

                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }

        protected void rptrStatus_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                _objPlVehicle.VehicleId = this.ID.Value;
                string message = _objBlVehicle.DeleteVendor(_objPlVehicle);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }


        protected void bSave_Click(object sender, EventArgs e)
        {
            int vendorid = 0, tankertypeid = 0;
            int buildingid = 0;
            if (!Common.DDVal(ddlVendor, out vendorid))
            {
                NotifyMessages("Please Select Vendor", Common.ErrorType.Error);
                return;
            }
            if (!Common.DDVal(ddtanker, out tankertypeid))
            {
                NotifyMessages("Please Select Tanker", Common.ErrorType.Error);
                return;
            }
            if (!Common.DDVal(ddlbuilding, out buildingid))
            {
                NotifyMessages("Please Select Default Building", Common.ErrorType.Error);
                return;
            }
            if (tRegnNo.Text == string.Empty)
            {
                NotifyMessages("Please enter the Reg. No", Common.ErrorType.Error);
                return;
            }
            if (tCapacity.Text == string.Empty)
            {
                NotifyMessages("Please enter the Capacity", Common.ErrorType.Error);
                return;
            }
            _objPlVehicle.CompanyID = UserSession.CompanyIDCurrent;
            _objPlVehicle.LocationID = UserSession.LocationIDCurrent;
            _objPlVehicle.VehicleRegno = this.tRegnNo.Text.Trim();
            _objPlVehicle.Capacity = this.tCapacity.Text.Trim();
            _objPlVehicle.VendorId = vendorid;
            _objPlVehicle.TankerId = tankertypeid;
            _objPlVehicle.Cuid = UserSession.UserID;
            _objPlVehicle.BuildingID = buildingid;
            try
            {
                if (this.IsAdd)
                {
                    _objPlVehicle.VehicleId = 0;
                    string[] Values = _objBlVehicle.InsertUpdateVehicle(_objPlVehicle);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Error);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else if (this.ID != null)
                {
                    _objPlVehicle.VehicleId = this.ID.Value;
                    string[] Values = _objBlVehicle.InsertUpdateVehicle(_objPlVehicle);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Error);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }
    }
}