﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerPL;

namespace iTanker
{
    public partial class dashboard : PageBase
    {
        PLVehicle _objPlVehicle = new PLVehicle();
        BLVehicle _objBlVehicle = new BLVehicle();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Setheadertextdetails();
                    LoadVendor();
                    string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("dd/MM/yyyy");
                    DateTime firstOfNextMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1);
                    string lastOfThisMonth = DateTime.Today.ToString("dd/MM/yyyy");
                    this.txtfromdate.Text = dFirstDayOfThisMonth;
                    this.txttodate.Text = lastOfThisMonth;
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }

        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "dashboard";
                if (submenu.Trim() == string.Empty)
                    submenu = "dashboard";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        public void LoadVendor()
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;

                _objPlVehicle.CompanyID = UserSession.CompanyIDCurrent;
                _objPlVehicle.LocationID = UserSession.LocationIDCurrent;
                DataSet Ds = new DataSet();
                Ds = _objBlVehicle.GetVendorDashboard(_objPlVehicle);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    ClearDatas();

                    rptrvendor.DataSource = Ds;
                    rptrvendor.DataBind();
                }
                else
                {
                    ClearDatas();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }

        }

        private void ClearDatas()
        {
            rptrvendor.DataSource = null;
            rptrvendor.DataBind();

            rptrtype.DataSource = null;
            rptrtype.DataBind();

            rptrInOut.DataSource = null;
            rptrInOut.DataBind();

            rptrvehicleSystem.DataSource = null;
            rptrvehicleSystem.DataBind();

            lblrawdatas.Text = string.Empty;
            lblvendorname.Text = string.Empty;
            lblinoutdet.Text = string.Empty;
        }

        protected void rptrvendor_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;

                int vendorid = Convert.ToInt32(e.CommandArgument);
                BindTypeDetails(vendorid);
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindTypeDetails(int vendorid)
        {
            try
            {
                _objPlVehicle.VendorId = vendorid;
                DataSet Ds = new DataSet();
                Ds = _objBlVehicle.GetTankertypeDashboard(_objPlVehicle);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    rptrtype.DataSource = null;
                    rptrtype.DataBind();

                    rptrInOut.DataSource = null;
                    rptrInOut.DataBind();

                    rptrvehicleSystem.DataSource = null;
                    rptrvehicleSystem.DataBind();

                    lblrawdatas.Text = string.Empty;
                    lblinoutdet.Text = string.Empty;

                    rptrtype.DataSource = Ds;
                    rptrtype.DataBind();

                    string vendorname = Ds.Tables[0].Rows[0]["VendorName"].ToString();
                    lblvendorname.Text = vendorname;
                }
                else
                {
                    ClearDatas();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void rptrtype_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;

                string ids = e.CommandArgument.ToString();

                BindInOutDet(ids);
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindInOutDet(string ids)
        {
            string[] typeIds = ids.Split(',');

            int vendorid = 0, tankerid = 0;
            string capacity = string.Empty;

            if (typeIds.Length > 2)
            {
                vendorid = Convert.ToInt32(typeIds[0]);
                tankerid = Convert.ToInt32(typeIds[1]);
                capacity = typeIds[2];
            }

            _objPlVehicle.VendorId = vendorid;
            _objPlVehicle.TankerId = tankerid;
            _objPlVehicle.Capacity = capacity;

            IFormatProvider culture = new CultureInfo("en-US", true);
            DateTime fromdate = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", culture);


            DateTime todate = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", culture);

            _objPlVehicle.Fromdate = fromdate;
            _objPlVehicle.Todate = todate;

            DataSet Ds = new DataSet();
            Ds = _objBlVehicle.GetInOutDet(_objPlVehicle);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrInOut.DataSource = null;
                rptrInOut.DataBind();

                rptrvehicleSystem.DataSource = null;
                rptrvehicleSystem.DataBind();

                lblrawdatas.Text = string.Empty;

                rptrInOut.DataSource = Ds;
                rptrInOut.DataBind();

                lblinoutdet.Text = "Tanker In/Tanker Out";
            }
            else
            {
                ClearDatas();
            }
        }

        protected void rptrInOut_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    LinkButton lnkin = (LinkButton)e.Item.FindControl("lnkin");
                    LinkButton lnkOut = (LinkButton)e.Item.FindControl("lnkOut");
                    if (lnkin != null && lnkOut != null)
                    {
                        if (lnkin.Text == "0")
                        {
                            lnkin.Enabled = false;
                        }
                        if (lnkOut.Text == "0")
                        {
                            lnkOut.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }

        }

        protected void rptrInOut_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;

                string ids = e.CommandArgument.ToString();

                string[] typeIds = ids.Split(',');

                int vendorid = 0, tankerid = 0;
                string capacity = string.Empty;

                if (typeIds.Length > 2)
                {
                    vendorid = Convert.ToInt32(typeIds[0]);
                    tankerid = Convert.ToInt32(typeIds[1]);
                    capacity = typeIds[2];
                }

                _objPlVehicle.VendorId = vendorid;
                _objPlVehicle.TankerId = tankerid;
                _objPlVehicle.Capacity = capacity;
                _objPlVehicle.Type = e.CommandName;

                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime fromdate = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", culture);

                DateTime todate = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", culture);

                _objPlVehicle.Fromdate = fromdate;
                _objPlVehicle.Todate = todate;
                DataSet Ds = new DataSet();
                Ds = _objBlVehicle.GetRawData(_objPlVehicle);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    rptrvehicleSystem.DataSource = Ds;
                    rptrvehicleSystem.DataBind();

                    lblrawdatas.Text = "Tanker "+ e.CommandName + " - Details";
                }
                else
                {
                    rptrvehicleSystem.DataSource = null;
                    rptrvehicleSystem.DataBind();

                    lblrawdatas.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }


        }

        protected void imgdownloadexcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptrvehicleSystem.Items.Count > 0)
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=InOutDetails.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";

                    System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                    //     Your Repeater Name Mine is "Rep"
                    rptrvehicleSystem.RenderControl(htmlWrite);
                    Response.Write("<table>");
                    Response.Write(stringWrite.ToString());
                    Response.Write("</table>");
                    Response.End();
                    Response.End();
                }
                else
                {
                    NotifyMessages("No Record Found", Common.ErrorType.Error);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            try
            {
                divmsg.Visible = true;
                lblError.Text = message;
                if (et == Common.ErrorType.Error)
                {
                    divmsg.Attributes.Add("class", "alert alert-danger");
                }
                else if (et == Common.ErrorType.Information)
                {
                    divmsg.Attributes.Add("class", "alert alert-success");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void btnvendor_Click(object sender, EventArgs e)
        {
            int vendorid = Convert.ToInt32(hdnvendorid.Value);
            BindTypeDetails(vendorid);
        }

        protected void btnIds_Click(object sender, EventArgs e)
        {
            string ids = hdnIds.Value.ToString();
            BindInOutDet(ids);
        }

        protected void btngo_Click(object sender, EventArgs e)
        {
            ClearDatas();
            LoadVendor();
        }
    }
}