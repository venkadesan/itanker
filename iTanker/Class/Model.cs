﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI.WebControls.Expressions;

namespace iTanker.Class
{
    public class Model
    {
        public class StatusData
        {
            public bool Status { set; get; }
            public string Message { set; get; }
        }

        public class NewVehicleStatusData
        {
            public bool Status { set; get; }
            public string Message { set; get; }
            public int VehicleId { get; set; }
        }

        public class VehicleStatusData
        {
            public bool Status { set; get; }
            public string Message { set; get; }
            public string Guid { set; get; }
        }

        public class ResponseData
        {
            public bool Status { set; get; }
            public string Message { set; get; }
            public object Data { set; get; }
        }

        public class ResponseDataNew
        {
            public object Data { set; get; }
        }

        public class User
        {
            public int UserID { get; set; }
            public int CompanyID { get; set; }
            public int LocationID { get; set; }
            public string CompanyName { get; set; }
            public string CompanyShortName { get; set; }
            public string LocationName { get; set; }
            public string LocationShortName { get; set; }
            public bool Status { set; get; }
            public string Message { set; get; }
        }

        public class VehicleData
        {
            public int VehicleId { get; set; }
            public string RegNo { get; set; }
            public string Capacity { get; set; }
            public string VendorName { get; set; }
            public string CommodityType { get; set; }
            public int VendorId { get; set; }
            public int TankerId { get; set; }
            public int DefaultBuildingId { get; set; }
            public bool Status { set; get; }
            public string Message { set; get; }
        }

        public class BuildingData
        {
            public long BuildingID { get; set; }
            public string BuildingName { set; get; }
            public string BuildingShortName { set; get; }
        }

        public class InVehicleData
        {
            public int VehicleId { get; set; }
            public string RegNo { get; set; }
            public string Capacity { get; set; }
            public string VendorName { get; set; }
            public int TokkenId { get; set; }
            public string InTime { get; set; }
            public string BuildingName { get; set; }
            public string ShowInDate { get; set; }
            public string ShowInTime { get; set; }
            public int VendorId { get; set; }
            public int BuildingId { get; set; }
            public string BuildingShortName { get; set; }
        }


    }
}