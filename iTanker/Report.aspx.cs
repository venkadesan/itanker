﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTanker.Code;
using iTankerBL;
using iTankerPL;

namespace iTanker
{
    public partial class Report : PageBase
    {
        PLVehicleSystem _objPlVehicleSystem = new PLVehicleSystem();
        BLVehicleSystem _objBlVehicleSystem = new BLVehicleSystem();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("dd/MM/yyyy");
                DateTime firstOfNextMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1);
                string lastOfThisMonth = DateTime.Today.ToString("dd/MM/yyyy");
                this.tfromdate.Text = dFirstDayOfThisMonth;
                this.ttoDate.Text = lastOfThisMonth;

                BindVendor();
                BindTanker();
                LoadDetails();
                Setheadertextdetails();
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Report";
                if (submenu.Trim() == string.Empty)
                    submenu = "Report";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindVendor()
        {
            try
            {
                PLVendor _objPlVendor = new PLVendor();
                BLVendor _objBlVendor = new BLVendor();

                DataSet dsVendor = new DataSet();
                _objPlVendor.CompanyID = UserSession.CompanyIDCurrent;
                _objPlVendor.LocationID = UserSession.LocationIDCurrent;
                _objPlVendor.VendorId = 0;
                dsVendor = _objBlVendor.GetVendor(_objPlVendor);
                if (dsVendor.Tables.Count > 0 && dsVendor.Tables[0].Rows.Count > 0)
                {
                    ddlVendor.DataSource = dsVendor.Tables[0];
                    ddlVendor.DataValueField = "VendorID";
                    ddlVendor.DataTextField = "VendorName";
                    ddlVendor.DataBind();

                    ddlVendor.Items.Insert(0, new ListItem("All", "0"));
                }
                else
                {
                    ddlVendor.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindTanker()
        {
            try
            {
                PLTankerType _objPlTankerType = new PLTankerType();
                BLTankerType _objBlTankerType = new BLTankerType();

                DataSet dsVendor = new DataSet();
                _objPlTankerType.CompanyID = UserSession.CompanyIDCurrent;
                _objPlTankerType.LocationID = UserSession.LocationIDCurrent;
                _objPlTankerType.TypeId = 0;
                dsVendor = _objBlTankerType.GetTankerType(_objPlTankerType);
                if (dsVendor.Tables.Count > 0 && dsVendor.Tables[0].Rows.Count > 0)
                {
                    ddtanker.DataSource = dsVendor.Tables[0];
                    ddtanker.DataValueField = "TypeID";
                    ddtanker.DataTextField = "TankerType";
                    ddtanker.DataBind();

                    ddtanker.Items.Insert(0, new ListItem("All", "0"));
                }
                else
                {
                    ddtanker.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        public string DayCalculation(object minute)
        {
            try
            {

                int iValues = int.Parse(minute.ToString());

                TimeSpan span = TimeSpan.FromSeconds(iValues);
                string result = String.Format("{0} Days : {1} Hrs : {2} Mins : {3} Sec",
    span.Days, span.Hours, span.Minutes, span.Seconds);

                //if (iValues < 0)
                //    iValues = -1 * iValues;

                //int iDays = iValues / (24 * 60 * 60);

                //iValues = int.Parse(Math.IEEERemainder(iValues, (24 * 60)).ToString());
                //int iHours = iValues / (60 * 60);

                //iValues = int.Parse(Math.IEEERemainder(iValues, 60).ToString());
                //int iMinutes = iValues / 60;

                //string result = iDays.ToString() + "Day " + iHours.ToString() + " Hrs " + iMinutes.ToString() + " Mins";
                return result;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
                return "0";
            }
        }

        private void LoadDetails()
        {
            try
            {
                int vendorid = 0, tankerid = 0;
                if (!Common.DDVal(ddlVendor, out vendorid))
                {
                    NotifyMessages("Select Vendor", Common.ErrorType.Error);
                    return;
                }
                if (!Common.DDVal(ddtanker, out tankerid))
                {
                    NotifyMessages("Select Tanker", Common.ErrorType.Error);
                    return;
                }

                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime fromdate = DateTime.ParseExact(tfromdate.Text, "dd/MM/yyyy", culture);

                DateTime todate = DateTime.ParseExact(ttoDate.Text, "dd/MM/yyyy", culture);

                _objPlVehicleSystem.FromDate = fromdate;
                _objPlVehicleSystem.ToDate = todate;
                _objPlVehicleSystem.VendorId = vendorid;
                _objPlVehicleSystem.CommdityType = tankerid;
                _objPlVehicleSystem.CompanyID = UserSession.CompanyIDCurrent;
                _objPlVehicleSystem.LocationID = UserSession.LocationIDCurrent;
                DataSet dsVehilcleDet = new DataSet();
                dsVehilcleDet = _objBlVehicleSystem.GetReportDetails(_objPlVehicleSystem);
                if (dsVehilcleDet != null && dsVehilcleDet.Tables.Count > 0 && dsVehilcleDet.Tables[0].Rows.Count > 0)
                {
                    rptrvehicleSystem.DataSource = dsVehilcleDet.Tables[0];
                    rptrvehicleSystem.DataBind();
                }
                else
                {
                    rptrvehicleSystem.DataSource = null;
                    rptrvehicleSystem.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            try
            {
                divmsg.Visible = true;
                lblError.Text = message;
                if (et == Common.ErrorType.Error)
                {
                    divmsg.Attributes.Add("class", "alert alert-danger");
                }
                else if (et == Common.ErrorType.Information)
                {
                    divmsg.Attributes.Add("class", "alert alert-success");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void lnksearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;
                LoadDetails();
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void lnkdownload_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;
                if (rptrvehicleSystem.Items.Count > 0)
                {
                    DataTable dtcfsImport = new DataTable();
                    dtcfsImport.Columns.Add("Sl.No");

                    dtcfsImport.Columns.Add("Vendor Name");
                    dtcfsImport.Columns.Add("Commodity Type");
                    dtcfsImport.Columns.Add("Reg. No");
                    dtcfsImport.Columns.Add("Capacity");
                    dtcfsImport.Columns.Add("Building");
                    dtcfsImport.Columns.Add("In Time");
                    dtcfsImport.Columns.Add("Out Time");
                    dtcfsImport.Columns.Add(" Duration");
                    dtcfsImport.Columns.Add("System Date");

                    foreach (RepeaterItem ricfsImport in rptrvehicleSystem.Items)
                    {
                        Label count = (Label)ricfsImport.FindControl("count");

                        Label VendorName = (Label)ricfsImport.FindControl("VendorName");
                        Label TankerType = (Label)ricfsImport.FindControl("TankerType");
                        Label VehicleRegNo = (Label)ricfsImport.FindControl("VehicleRegNo");
                        Label Capacity = (Label)ricfsImport.FindControl("Capacity");
                        Label BuildingName = (Label)ricfsImport.FindControl("BuildingName");
                        Label InTime = (Label)ricfsImport.FindControl("InTime");
                        Label OutTime = (Label)ricfsImport.FindControl("OutTime");
                        Label TotalSeconds = (Label)ricfsImport.FindControl("TotalSeconds");
                        Label Date = (Label)ricfsImport.FindControl("Date");

                        if (count != null && VendorName != null && TankerType != null && VehicleRegNo != null &&
                            Capacity != null && BuildingName != null && InTime != null && OutTime != null &&
                            TotalSeconds != null && Date != null)
                        {
                            dtcfsImport.Rows.Add(count.Text,
                                VendorName.Text, TankerType.Text, VehicleRegNo.Text,
                                Capacity.Text, BuildingName.Text, InTime.Text, OutTime.Text,
                                TotalSeconds.Text, Date.Text);
                        }
                    }

                    ExportExcel(dtcfsImport);
                }
                else
                {
                    NotifyMessages("No Record Found", Common.ErrorType.Error);
                }

            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private void ExportExcel(DataTable dtImport)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ReportDet" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                StringBuilder output = new StringBuilder();

                output.Append("<table border='1'><tbody><tr>");
                foreach (DataColumn dtcol in dtImport.Columns)
                {
                    output.Append("<th>" + dtcol.ColumnName + "</th>");
                }
                output.Append("</tr>");

                foreach (DataRow dr in dtImport.Rows)
                {
                    output.Append("<tr>");
                    for (int j = 0; j < dtImport.Columns.Count; j++)
                    {
                        output.Append("<td>" + Convert.ToString(dr[j]) + "</td>");
                    }
                    output.Append("</tr>");
                }
                output.Append("</tbody></table>");
                Response.Output.Write(output.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void tfromdate_TextChanged(object sender, EventArgs e)
        {
            LoadDetails();
        }

        protected void ttoDate_TextChanged(object sender, EventArgs e)
        {
            LoadDetails();
        }

        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDetails();
        }

        protected void ddtanker_TextChanged(object sender, EventArgs e)
        {
            LoadDetails();
        }
    }
}