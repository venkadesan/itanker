﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="dashboard.aspx.cs" Inherits="iTanker.dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <script>
        $(document).ready(function () {
            /* Initialize Datatables */
            $('#example-datatables').dataTable({
                "iDisplayLength": 100
            });
            $('.dataTables_filter input').attr('placeholder', 'Search');


            $('#lidashboard').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#lidashboard").parent().parent().parent().addClass("active");
        });

        function addRowHandlers(vendorid) {
            document.getElementById('<%= hdnvendorid.ClientID%>').value = vendorid;
            document.getElementById('<%= btnvendor.ClientID%>').click();
        }

        function addtypeRowHandlers(ids) {
            document.getElementById('<%= hdnIds.ClientID%>').value = ids;
            document.getElementById('<%= btnIds.ClientID%>').click();
        }

    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Dashboard"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Dashboard"></asp:Literal></li>
    </ul>
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <h3 class="page-header page-header-top" style="height: 50px;">
        <div class="form-group">
            <div class="col-md-2">
            </div>
            <label class="control-label col-md-2" for="example-input-datepicker">
                Start Date</label>
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control input-datepicker"></asp:TextBox>
            </div>
            <label class="control-label col-md-2" for="example-input-datepicker">
                End Date</label>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control input-datepicker"></asp:TextBox>
            </div>
               <div class="col-md-2">
                <asp:Button ID="btngo" runat="server" CssClass="btn btn-success" Text="Go" 
                       onclick="btngo_Click" />
            </div>
        </div>
    </h3>
    <div class="dash-tiles row">
        <!-- Column 1 of Row 1 -->
        <div class="col-sm-4">
            <!-- Total Users Tile -->
            <div class="dash-tile dash-tile-ocean clearfix animation-pullDown" style="background: #C9A188;">
                <div class="dash-tile-header">
                    Total Vendors
                </div>
                <div class="dash-tile-text" style="padding: 8px; float: left;">
                    <table style="font-size: 12px; color: aliceblue; text-align: left;" class="table table-bordered table-hover"
                        width="100%">
                        <asp:Repeater ID="rptrvendor" runat="server" DataSourceID="" OnItemCommand="rptrvendor_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr class="submenu new-color">
                                        <th style="width: 68%;">
                                            Vendor
                                        </th>
                                        <th>
                                            Total Vehicles
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr onclick="addRowHandlers(<%# Eval("VendorID") %>)" style="cursor: pointer;">
                                    <td>
                                        <asp:LinkButton ID="lnkvendorname" runat="server" Text='<%# Eval("VendorName") %>'
                                            Style="color: black;" CommandArgument='<%# Eval("VendorID") %>'></asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton Style="color: black;" ID="lnkCount" runat="server" Text='<%# Eval("Count") %>'
                                            CommandArgument='<%# Eval("VendorID") %>'></asp:LinkButton>
                                    </td>
                                    <asp:HiddenField ID="hdnvendorid" Value='<%# Eval("VendorID") %>' runat="server" />
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
            <asp:HiddenField ID="hdnvendorid" Value="0" runat="server" />
            <asp:Button ID="btnvendor" Style="display: none;" runat="server" Text="" OnClick="btnvendor_Click" />
        </div>
        <!-- END Column 1 of Row 1 -->
        <!-- Column 2 of Row 1 -->
        <div class="col-sm-4">
            <!-- Total Sales Tile -->
            <div class="dash-tile dash-tile-flower clearfix animation-pullDown" style="background: #FFC100;">
                <div class="dash-tile-header">
                    <asp:Label ID="lblvendorname" runat="server" Text=""></asp:Label> 
                </div>
                <div class="dash-tile-text" style="padding: 8px; float: left;">
                    <table style="font-size: 12px; color: aliceblue; text-align: left;" class="table table-bordered table-hover"
                        width="100%">
                        <asp:Repeater ID="rptrtype" runat="server" DataSourceID="" OnItemCommand="rptrtype_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr class="submenu new-color">
                                        <th style="width: 68%;">
                                            Tanker Type (Capacity in litres)
                                        </th>
                                        <th>
                                            Total Vehicles
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr onclick="addtypeRowHandlers('  <%# Eval("IDs") %>  ')" style="cursor: pointer;">
                                    <td>
                                        <asp:LinkButton ID="lnkvendorname" runat="server" Text='<%# Eval("Type") %>' Style="color: black;"
                                            CommandArgument='<%# Eval("IDs") %>'></asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton Style="color: black;" ID="lnkCount" runat="server" Text='<%# Eval("Count") %>'
                                            CommandArgument='<%# Eval("IDs") %>'></asp:LinkButton>
                                    </td>
                                    <asp:HiddenField ID="hdnIds" Value='<%# Eval("IDs") %>' runat="server" />
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>
                <asp:HiddenField ID="hdnIds" Value="0,0,0" runat="server" />
                <asp:Button ID="btnIds" runat="server" Style="display: none;" Text="Button" OnClick="btnIds_Click" />
            </div>
            <!-- END Total Downloads Tile -->
        </div>
        <!-- END Column 2 of Row 1 -->
        <!-- Column 3 of Row 1 -->
        <div class="col-sm-4">
            <!-- Popularity Tile -->
            <div class="dash-tile dash-tile-oil clearfix animation-pullDown" style="background: #22beef;">
                <div class="dash-tile-header">
                    <asp:Label ID="lblinoutdet" runat="server" ></asp:Label>  
                </div>
                <div class="dash-tile-text" style="padding: 8px; float: left;">
                    <table style="font-size: 12px; color: aliceblue; text-align: left;" class="table table-bordered table-hover"
                        width="100%">
                        <asp:Repeater ID="rptrInOut" runat="server" DataSourceID="" OnItemCommand="rptrInOut_ItemCommand"
                            OnItemDataBound="rptrInOut_ItemDataBound">
                            <HeaderTemplate>
                                <thead>
                                    <tr class="submenu new-color">
                                        <th style="width: 74%;">
                                            Tanker In
                                        </th>
                                        <th>
                                            Tanker Out
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkin" runat="server" Text='<%# Eval("In") %>' Style="color: black;"
                                            CommandArgument='<%# Eval("IDs") %>' CommandName="In"></asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkOut" runat="server" Text='<%# Eval("Out") %>' Style="color: black;"
                                            CommandArgument='<%# Eval("IDs") %>' CommandName="Out"></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Column 3 of Row 1 -->
        <!-- Column 4 of Row 1 -->
        <div class="col-sm-3" style="display: none;">
            <!-- RSS Subscribers Tile -->
            <div class="dash-tile dash-tile-balloon clearfix animation-pullDown">
                <div class="dash-tile-header">
                    Total Users
                </div>
                <div class="dash-tile-text">
                    265k</div>
            </div>
            <!-- END RSS Subscribers Tile -->
            <!-- Total Tickets Tile -->
        </div>
        <!-- END Column 4 of Row 1 -->
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            <asp:Label ID="lblrawdatas" runat="server" Text=""></asp:Label>
            <asp:LinkButton ID="imgdownloadexcel" Style="float: right;" ToolTip="Download to Excel"
                CssClass="gi gi-download_alt" runat="server" Height="30px" Width="30px" OnClick="imgdownloadexcel_Click" />
        </h4>
        <div class="form-box-content">
            <div class="row">
                <table id="example-datatables" class="table table-striped table-bordered table-hover">
                    <asp:Repeater ID="rptrvehicleSystem" runat="server" DataSourceID="">
                        <HeaderTemplate>
                            <thead>
                                <tr class="submenu new-color">
                                    <th>
                                        S.No
                                    </th>
                                    <th>
                                        System Date
                                    </th>
                                    <th>
                                        Vendor Name
                                    </th>
                                    <th>
                                        Commodity Type
                                    </th>
                                    <th>
                                        Reg. No
                                    </th>
                                    <th>
                                        Tanker Type Capacity (in litres)
                                    </th>
                                    <th>
                                        Building
                                    </th>
                                    <th>
                                        Tanker In Time
                                    </th>
                                    <th>
                                        Tanker Out Time
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Date") %>' ToolTip='<%# Eval("Date") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TankerType") %>' ToolTip='<%# Eval("TankerType") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("VehicleRegNo") %>' ToolTip='<%# Eval("VehicleRegNo") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Capacity") %>' ToolTip='<%# Eval("Capacity") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("BuildingName") %>' ToolTip='<%# Eval("BuildingName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("InTime") %>' ToolTip='<%# Eval("InTime") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("OutTime") %>' ToolTip='<%# Eval("OutTime") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
