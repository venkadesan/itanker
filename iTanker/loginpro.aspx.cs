﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTanker.Code;
using Kowni.Helper;
using Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;
using iTanker.Code;
using System.Data;

namespace iTanker
{
    public partial class loginpro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                MCB.BLSingleSignOn objSingleSignOn = new MCB.BLSingleSignOn();
                MCB.BLAnnouncement objAnnouncement = new MCB.BLAnnouncement();
                BLUser objUser = new BLUser();

                UserSession.FromFramework = false;
                if (Request.QueryString["token"] != null && Request.QueryString["token"].ToString() != string.Empty)
                {
                    if (UserSession.RoleID > 0)
                    {
                        HttpContext.Current.Session.Clear();
                    }

                    int RoleID = 0;
                    int UserID = 1;
                    int CompanyID = 0;
                    string CompanyName = string.Empty;
                    int LocationID = 0;
                    string LocationName = string.Empty;
                    int GroupID = 0;
                    int LanguageID = 0;
                    string UserFName = string.Empty;
                    string UserLName = string.Empty;
                    string UserMailID = string.Empty;
                    string ThemeFolderPath = string.Empty;

                    if (!objSingleSignOn.VerifyLoginToken(Request.QueryString["token"].ToString(), out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
                    {
                    }
                    else
                    {
                        UserSession.FromFramework = false;

                        UserSession.CountryID = 1;
                        UserSession.RoleID = RoleID;
                        UserSession.UserID = UserID;

                        UserSession.CompanyIDUser = CompanyID;
                        UserSession.LocationIDUser = LocationID;

                        UserSession.CompanyName = CompanyName;
                        UserSession.CompanyIDCurrent = CompanyID;

                        UserSession.LocationName = LocationName;
                        UserSession.LocationIDCurrent = LocationID;

                        UserSession.GroupID = GroupID;
                        UserSession.LanguageID = LanguageID;
                        UserSession.UserFirstName = UserFName;
                        UserSession.UserLastName = UserLName;
                        UserSession.ThemeFolderPath = ThemeFolderPath;
                        UserSession.FromFramework = true;

                        DataSet dsAnn = new DataSet();
                        dsAnn = objAnnouncement.GetAnnouncement(UserSession.CompanyIDCurrent, Convert.ToInt32(Kowni.Common.BusinessLogic.BLMenu.ToolID.MasterFramework));
                        if (dsAnn.Tables.Count > 0 && dsAnn.Tables[0].Rows.Count > 0)
                        {
                            if (dsAnn.Tables[0].Rows[0]["AnnouncementType"].ToString() == "1")
                            {
                                UserSession.Announcement = dsAnn.Tables[0].Rows[0]["Announcement"].ToString();
                            }
                        }
                        else
                        {
                            UserSession.Announcement = string.Empty;
                        }
                        Response.Redirect("dashboard.aspx", true);
                    }
                }
                else
                {
                    Response.Redirect("login.aspx", true);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }
}