﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTankerPL
{
    public class PLVehicleSystem : PLCommon
    {
        public int VehicleId { get; set; }
        public int TokenId { get; set; }
        public DateTime SystemTime { get; set; }
        public string Type { get; set; }
        public DateTime SystemDate { get; set; }
        public int BuildingId { get; set; }

        public int VendorId { get; set; }
        public int CommdityType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string InGuid { get; set; }
    }
}
