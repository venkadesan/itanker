﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTankerPL
{
   public class PLVehicle : PLCommon
    {
       public int VehicleId { get; set; }
       public string VehicleRegno { get; set; }
       public string Capacity { get; set; }
       public int VendorId { get; set; }
       public int TankerId { get; set; }
       public int BuildingID { get; set; }

       public DateTime Fromdate { get; set; }
       public DateTime Todate { get; set; }
       public string Type { get; set; }
    }
}
