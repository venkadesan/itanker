﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTankerPL
{
   public class PLTankerType : PLCommon
    {
       public int TypeId { get; set; }
       public string Type { get; set; }
       public string ShortType { get; set; }
    }
}
