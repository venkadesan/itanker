﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTankerPL
{
    public class PLThemeselection : PLCommon
    {
       public string PrimaryTheme { get; set; }
       public string SecondaryTheme { get; set; }
    }
}
