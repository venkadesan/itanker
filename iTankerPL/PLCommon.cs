﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTankerPL
{
    public class PLCommon
    {
        public int Cuid { get; set; }
        public int Muid { get; set; }
        public DateTime Cdate { get; set; }
        public DateTime Mdate { get; set; }
        public int CompanyID { get; set; }
        public int LocationID { get; set; }
        
    }
}
