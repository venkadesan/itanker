﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using iTankerDL;
using iTankerPL;

namespace iTankerBL
{
    public class BLVehicleSystem
    {
        private DLVehicleSystem _objVehicleSystem = new DLVehicleSystem();

        /// <summary>
        /// Purpose to get the Vehicle Systems
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetVehicleSystems(PLVehicleSystem vehicle)
        {
            try
            {
                return _objVehicleSystem.GetVehicleSystems(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the vehicle System
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public string[] InsertUpdateVehicle(PLVehicleSystem vehicle)
        {
            try
            {
                return _objVehicleSystem.InsertUpdateVehicleSystem(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the vehicle System
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public string[] InsertUpdateVehicleSystemWeb(PLVehicleSystem vehicle)
        {
            try
            {
                return _objVehicleSystem.InsertUpdateVehicleSystemWeb(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetReportDetails(PLVehicleSystem vehicle)
        {
            try
            {
                return _objVehicleSystem.GetReportDetails(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
