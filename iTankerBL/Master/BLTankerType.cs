﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using iTankerDL;
using iTankerPL;

namespace iTankerBL
{
    public class BLTankerType
    {
        private DLTankerType objType = new DLTankerType();

        /// <summary>
        /// Purpose to get the Tanker type
        /// </summary>
        /// <param name="tankerType"></param>
        /// <returns></returns>
        public DataSet GetTankerType(PLTankerType tankerType)
        {
            try
            {
                return objType.GetTankerType(tankerType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the tanker type
        /// </summary>
        /// <param name="tankerType"></param>
        /// <returns></returns>
        public string[] InsertUpdateTankerType(PLTankerType tankerType)
        {
            try
            {
                return objType.InsertUpdateTankerType(tankerType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to delete the tanker type
        /// </summary>
        /// <param name="tankerType"></param>
        /// <returns></returns>
        public string DeleteTankerType(PLTankerType tankerType)
        {
            try
            {
                return objType.DeleteTankerType(tankerType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
