﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using iTankerDL;
using iTankerPL;

namespace iTankerBL
{
    public class BLVehicle
    {
        private DLVehicle _objVehicle = new DLVehicle();

        /// <summary>
        /// Purpose to get the vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetVehicle(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.GetVehicle(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public string[] InsertUpdateVehicle(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.InsertUpdateVehicle(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to delete the vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public string DeleteVendor(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.DeleteVehicle(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to get the Building details by location ID
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetBuildingDetails(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.GetBuildingDetails(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Purpose to get the In-Vehicle Details by InDate and company and location
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetInVehiclesDetails(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.GetInVehiclesDetails(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to get the vendor details by company and location for dashboard 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetVendorDashboard(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.GetVendorDashboard(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetTankertypeDashboard(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.GetTankertypeDashboard(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetInOutDet(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.GetInOutDet(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetRawData(PLVehicle vehicle)
        {
            try
            {
                return _objVehicle.GetRawData(vehicle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
