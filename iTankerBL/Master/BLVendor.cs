﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using iTankerDL;
using iTankerPL;

namespace iTankerBL
{
    public class BLVendor
    {
        private DLVendor objVendor = new DLVendor();

        /// <summary>
        /// Purpose to get the vendor
        /// </summary>
        /// <param name="vendor"></param>
        /// <returns></returns>
        public DataSet GetVendor(PLVendor vendor)
        {
            try
            {
                return objVendor.GetVendor(vendor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the vendor
        /// </summary>
        /// <param name="vendor"></param>
        /// <returns></returns>
        public string[] InsertUpdateVendor(PLVendor vendor)
        {
            try
            {
                return objVendor.InsertUpdateVendor(vendor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to delete the vendor
        /// </summary>
        /// <param name="vendor"></param>
        /// <returns></returns>
        public string DeleteVendor(PLVendor vendor)
        {
            try
            {
                return objVendor.DeleteVendor(vendor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
