﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using iTankerDL;
using iTankerPL;

namespace iTankerBL
{
   public class BLThemeSelection
   {
       DLThemeSelection objDL = new DLThemeSelection();

       public string[] InsertUpdateColorThemes(PLThemeselection themeselection)
       {
           string[] Value = objDL.InsertUpdateColorThemes(themeselection);
           return Value;
       }

       public DataSet GetColorThemes(PLThemeselection themeselection)
       {
           DataSet Ds = new DataSet();
           Ds = objDL.GetColorThemes(themeselection);
           return Ds;
       }
    }
}
