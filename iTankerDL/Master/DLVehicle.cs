﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTankerPL;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;

namespace iTankerDL
{
    public class DLVehicle
    {
        /// <summary>
        /// Purpose to get the vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetVehicle(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetVehicle");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                db.AddInParameter(cmd, "@VehicleID", DbType.Int32, vehicle.VehicleId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the vendor type
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public string[] InsertUpdateVehicle(PLVehicle vehicle)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdateVehicle");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                db.AddInParameter(cmd, "@VehicleRegNo", DbType.String, vehicle.VehicleRegno);
                db.AddInParameter(cmd, "@Capacity", DbType.String, vehicle.Capacity);
                db.AddInParameter(cmd, "@VendorID", DbType.Int32, vehicle.VendorId);
                db.AddInParameter(cmd, "@TankerID", DbType.Int32, vehicle.TankerId);
                db.AddInParameter(cmd, "@VehicleID", DbType.Int32, vehicle.VehicleId);
                db.AddInParameter(cmd, "@DefaultBuildingID", DbType.Int32, vehicle.BuildingID);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddInParameter(cmd, "@UID", DbType.Int32, vehicle.Cuid);
                db.AddOutParameter(cmd, "@Status", DbType.String, 1000);
                db.AddOutParameter(cmd, "@newVehicleId", DbType.Int32, 20);
                int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                Values[2] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@newVehicleId"));
                return Values;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to delete the Vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>

        public string DeleteVehicle(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("DeleteVehicle");
                db.AddInParameter(cmd, "@VehicleId", DbType.Int32, vehicle.VehicleId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.ExecuteNonQuery(cmd);
                string results = string.Format(CultureInfo.CurrentCulture, "{0}",
                    db.GetParameterValue(cmd, "@ErrorMessage"));
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to get the Building details by location ID
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetBuildingDetails(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetBuildingDetails");
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Purpose to get the In-Vehicle Details by InDate and company and location
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetInVehiclesDetails(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetInVehicles");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                db.AddInParameter(cmd, "@InDate", DbType.DateTime, vehicle.Cdate);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Purpose to get the vendor details by company and location for dashboard
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetVendorDashboard(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_getVendor_Dashboard");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to get the vendor details by company and location for dashboard
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetTankertypeDashboard(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetTankertype_dashboard");
                db.AddInParameter(cmd, "@VendorID", DbType.Int32, vehicle.VendorId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to get the vendor details by company and location for dashboard
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetInOutDet(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetInOutDet");
                db.AddInParameter(cmd, "@VendorID", DbType.Int32, vehicle.VendorId);
                db.AddInParameter(cmd, "@TankerID", DbType.Int32, vehicle.TankerId);
                db.AddInParameter(cmd, "@Capacity", DbType.Int32, vehicle.Capacity);
                db.AddInParameter(cmd, "@FromDate", DbType.DateTime, vehicle.Fromdate);
                db.AddInParameter(cmd, "@ToDate", DbType.DateTime, vehicle.Todate);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to get the vendor details by company and location for dashboard
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetRawData(PLVehicle vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetRawDatas");
                db.AddInParameter(cmd, "@VendorID", DbType.Int32, vehicle.VendorId);
                db.AddInParameter(cmd, "@TankerID", DbType.Int32, vehicle.TankerId);
                db.AddInParameter(cmd, "@Capacity", DbType.Int32, vehicle.Capacity);
                db.AddInParameter(cmd, "@FromDate", DbType.DateTime, vehicle.Fromdate);
                db.AddInParameter(cmd, "@ToDate", DbType.DateTime, vehicle.Todate);
                db.AddInParameter(cmd, "@Type", DbType.String, vehicle.Type);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
