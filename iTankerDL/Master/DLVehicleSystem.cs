﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using iTankerPL;
using System.Data;
using iTankerPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace iTankerDL
{
    public class DLVehicleSystem
    {
        /// <summary>
        /// Purpose to get the vehicle system 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public DataSet GetVehicleSystems(PLVehicleSystem vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetVehicleSystems");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the vendor type
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public string[] InsertUpdateVehicleSystem(PLVehicleSystem vehicle)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertVehicleSystem");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                db.AddInParameter(cmd, "@TokenID", DbType.Int32, vehicle.TokenId);
                db.AddInParameter(cmd, "@Type", DbType.String, vehicle.Type);
                db.AddInParameter(cmd, "@Time", DbType.DateTime, vehicle.SystemTime);
                db.AddInParameter(cmd, "@Date", DbType.DateTime, vehicle.SystemDate);
                db.AddInParameter(cmd, "@BuildingID", DbType.Int32, vehicle.BuildingId);
                db.AddInParameter(cmd, "@VehicleID", DbType.Int32, vehicle.VehicleId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 20);
                db.AddInParameter(cmd, "@UID", DbType.Int32, vehicle.Cuid);
                db.AddInParameter(cmd, "@Guid", DbType.String, vehicle.InGuid);
                int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@Status"));
                return Values;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the vendor type
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public string[] InsertUpdateVehicleSystemWeb(PLVehicleSystem vehicle)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertVehicleSystemWeb");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);
                db.AddInParameter(cmd, "@TokenID", DbType.Int32, vehicle.TokenId);
                db.AddInParameter(cmd, "@InTime", DbType.DateTime, vehicle.SystemTime);
                db.AddInParameter(cmd, "@OutTime", DbType.DateTime, vehicle.SystemDate);
                db.AddInParameter(cmd, "@BuildingID", DbType.Int32, vehicle.BuildingId);
                db.AddInParameter(cmd, "@VehicleID", DbType.Int32, vehicle.VehicleId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 20);
                db.AddInParameter(cmd, "@UID", DbType.Int32, vehicle.Cuid);
                int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@Status"));
                return Values;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetReportDetails(PLVehicleSystem vehicle)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetReportDetails");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vehicle.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vehicle.LocationID);

                db.AddInParameter(cmd, "@VendorId", DbType.Int32, vehicle.VendorId);
                db.AddInParameter(cmd, "@TankerId", DbType.Int32, vehicle.CommdityType);
                db.AddInParameter(cmd, "@FromDate", DbType.DateTime, vehicle.FromDate);
                db.AddInParameter(cmd, "@ToDate", DbType.DateTime, vehicle.ToDate);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
