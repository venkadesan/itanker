﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTankerPL;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;

namespace iTankerDL
{
   public class DLVendor
    {

        /// <summary>
        /// Purpose to get the vendor type
        /// </summary>
        /// <param name="vendor"></param>
        /// <returns></returns>
       public DataSet GetVendor(PLVendor vendor)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetVendor");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vendor.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vendor.LocationID);
                db.AddInParameter(cmd, "@VendorID", DbType.Int32, vendor.VendorId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
       /// Purpose to insert/update the vendor type
        /// </summary>
       /// <param name="vendor"></param>
        /// <returns></returns>
       public string[] InsertUpdateVendor(PLVendor vendor)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdateVendor");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, vendor.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, vendor.LocationID);
                db.AddInParameter(cmd, "@VendorName", DbType.String, vendor.VendorName);
                db.AddInParameter(cmd, "@VendorAddress", DbType.String, vendor.VendorAddress);
                db.AddInParameter(cmd, "@VendorID", DbType.Int32, vendor.VendorId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddInParameter(cmd, "@UID", DbType.Int32, vendor.Cuid);
                db.AddOutParameter(cmd, "@Status", DbType.String, 1000);
                int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                Values[2] = Value.ToString();
                return Values;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
       /// Purpose to delete the vendor type
        /// </summary>
       /// <param name="vendor"></param>
        /// <returns></returns>

       public string DeleteVendor(PLVendor vendor)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("DeleteVendor");
                db.AddInParameter(cmd, "@TypeID", DbType.Int32, vendor.VendorId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.ExecuteNonQuery(cmd);
                string results = string.Format(CultureInfo.CurrentCulture, "{0}",
                    db.GetParameterValue(cmd, "@ErrorMessage"));
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
