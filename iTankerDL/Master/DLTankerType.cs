﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTankerPL;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;

namespace iTankerDL
{
    public class DLTankerType
    {
        /// <summary>
        /// Purpose to get the Tanker type
        /// </summary>
        /// <param name="tankerType"></param>
        /// <returns></returns>
        public DataSet GetTankerType(PLTankerType tankerType)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetTankerType");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, tankerType.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, tankerType.LocationID);
                db.AddInParameter(cmd, "@TypeID", DbType.Int32, tankerType.TypeId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to insert/update the tanker type
        /// </summary>
        /// <param name="tankerType"></param>
        /// <returns></returns>
        public string[] InsertUpdateTankerType(PLTankerType tankerType)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdateTankerType");
                db.AddInParameter(cmd, "@CompanyID", DbType.Int32, tankerType.CompanyID);
                db.AddInParameter(cmd, "@LocationID", DbType.Int32, tankerType.LocationID);
                db.AddInParameter(cmd, "@TankerType", DbType.String, tankerType.Type);
                db.AddInParameter(cmd, "@TankerShortname", DbType.String, tankerType.ShortType);
                db.AddInParameter(cmd, "@TypeID", DbType.Int32, tankerType.TypeId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddInParameter(cmd, "@UID", DbType.Int32, tankerType.Cuid);
                db.AddOutParameter(cmd, "@Status", DbType.String, 1000);
                int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                Values[2] = Value.ToString();
                return Values;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Purpose to delete the tanker type
        /// </summary>
        /// <param name="tankerType"></param>
        /// <returns></returns>

        public string DeleteTankerType(PLTankerType tankerType)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
                DbCommand cmd = db.GetStoredProcCommand("DeleteTankerType");
                db.AddInParameter(cmd, "@TypeID", DbType.Int32, tankerType.TypeId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.ExecuteNonQuery(cmd);
                string results = string.Format(CultureInfo.CurrentCulture, "{0}",
                    db.GetParameterValue(cmd, "@ErrorMessage"));
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
