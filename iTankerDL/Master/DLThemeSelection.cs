﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTankerPL;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
namespace iTankerDL
{
    public class DLThemeSelection
    {
        public DataSet GetColorThemes(PLThemeselection themeselection)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetColorThemes");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, themeselection.CompanyID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
        public string[] InsertUpdateColorThemes(PLThemeselection themeselection)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiAsstz");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdateColorThemes");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, themeselection.CompanyID);
            db.AddInParameter(cmd, "@PrmaryTheme", DbType.String, themeselection.PrimaryTheme.Trim());
            db.AddInParameter(cmd, "@SecondaryTheme", DbType.String, themeselection.SecondaryTheme.Trim());
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, themeselection.Cuid);
            db.AddOutParameter(cmd, "@Status", DbType.String, 1000);
            int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
            Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
            Values[2] = Value.ToString();
            return Values;
        }


    }
}
